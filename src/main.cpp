//
// main2.cpp for  in /home/grandr_r/Cpp_Prog/cpp_indie_studio
//
// Made by grandr_r
// Login   <grandr_r@epitech.net>
//
// Started on  Fri Apr 29 12:49:55 2016 grandr_r
// Last update Sun May 22 12:32:25 2016 
//

#include "../include/BaseFrame.hpp"

int		main()
{
  BaseFrame	base;
  IMenu		menu;

  menu.load();
  base.play(menu);
  return (0);
}
