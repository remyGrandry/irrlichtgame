//
// ICamera.hpp for  in /home/grandr_r/Cpp_Prog/cpp_indie_studio
//
// Made by 
// Login   <grandr_r@epitech.net>
//
// Started on  Sat May 21 18:55:25 2016 
// Last update Sun May 22 14:29:30 2016 
//

#ifndef ICAMERA_HPP_
# define ICAMERA_HPP_

#include <irrlicht.h>

class 				ICamera
{
  irr::core::vector3df		cameraPos;
  irr::core::vector3df		cameraTarget;
  irr::scene::ICameraSceneNode	*camera;
public:
  ICamera() {};
  ICamera(int x, int y, int z, int x2, int y2, int z2, irr::scene::ISceneManager *smgr)
  {
    cameraPos = irr::core::vector3df(x, y, z);
    cameraTarget = irr::core::vector3df(x2, y2, z2);
    camera = smgr->addCameraSceneNode();
  };

  void				updatePosition(irr::core::vector3df const &vect)
  {
    camera->setPosition(cameraPos);
  };

  void				updateTarget(irr::core::vector3df const &vect)
  {
    camera->setTarget(cameraTarget);
  };

  irr::core::vector3df		&getPos()
  {
    return (cameraPos);
  };

  irr::core::vector3df		&getTarget()
  {
    return (cameraTarget);
  };

  ~ICamera() {};
};

#endif
