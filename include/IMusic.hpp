//
// IMusic.hpp for  in /home/grandr_r/Cpp_Prog/cpp_indie_studio
//
// Made by 
// Login   <grandr_r@epitech.net>
//
// Started on  Sun May 22 16:06:39 2016 
// Last update Sat May 28 17:10:43 2016 
//

#ifndef IMUSIC_HPP_
#define IMUSIC_HPP_

#include <irrKlang.h>
#include <thread>
#include <mutex>
#include <iostream>
#include <irrlicht.h>

class 			IMusic
{
  irrklang::ISoundEngine* engine;
  irrklang::ISound *	music;
  bool			_isMusic;
  std::string const	allSound[4] = {"ressources/treasure.wav", "ressources/opendoor.wav", "ressources/opendoor.wav", "ressources/welcome.wav"};
  void			test(const char * name, bool autoRepeat = false, bool isMusic = false)
  {
    _isMusic = isMusic;
    engine = irrklang::createIrrKlangDevice();
    if (isMusic == false)
      return;
    engine->setSoundVolume(0.5);
    if (engine)
      music = engine->play2D(name, autoRepeat);
  };
public:
  IMusic(const char * name, bool autoRepeat = false, bool isMusic = false)
  {
    std::thread first(&IMusic::test, this, name, autoRepeat, isMusic);
    first.join();
  };

  void		playLastSoundThread(const char *name, float volume, irr::IrrlichtDevice *device)
  {
    if (_isMusic == true)
      {
	engine->stopAllSounds();
	engine->setSoundVolume(volume);
	engine->play2D(name);
	while (engine->isCurrentlyPlaying(name));
      }
    else
      sleep(3);
    device->closeDevice();
  };

  void		playLastSound(const char *name, float volume, irr::IrrlichtDevice *device)
  {
    std::thread first(&IMusic::playLastSoundThread, this, name, volume, device);
    first.join();
  };

  void		playSoundThread(int sound)
  {
    if (_isMusic == false)
      return;
    if (sound == 1 || sound == 2)
      engine->setSoundVolume(5);
    //engine->stopAllSounds();
    if (!engine->isCurrentlyPlaying(allSound[sound].c_str()))
      engine->play2D(allSound[sound].c_str());
    engine->setSoundVolume(0.5);
  };

  void		playSound(int sound)
  {
    std::thread first(&IMusic::playSoundThread, this, sound);
    first.join();
  };

  void		pauseSound()
  {
    engine->setSoundVolume(0);
  };

  void		playSoundAgain()
  {
    engine->setSoundVolume(0.5);
  };

  ~IMusic()
  {
    if (engine)
      engine->drop();
  };
};

#endif
