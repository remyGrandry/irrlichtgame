
//
// Map.hpp for  in /home/grandr_r/Cpp_Prog/cpp_indie_studio
//
// Made by 
// Login   <grandr_r@epitech.net>
//
// Started on  Wed May 25 12:56:30 2016 
// Last update Tue May 31 12:43:04 2016 
//

#ifndef MAP_HPP_
# define MAP_HPP_

#include <irrlicht.h>
#include <vector>
#include "Door.hpp"
#include "IMusic.hpp"
#include "ICharacter.hpp"
#include "Key.hpp"
#include "IEnemy.hpp"
#include "GetFromFile.hpp"
#include "Spawner.hpp"
#include "Meal.hpp"
#include "Chest.hpp"
#include "Exit.hpp"

class 				Map
{
  std::vector<Door *>		allDoors;
  std::vector<Key *>		allKeys;
  std::vector<Spawner *>	allSpawners;
  std::vector<Chest *>		allChest;
  std::vector<Meal *>		allMeal;
  Exit				exit;
public:
  Map() {};

  void				initMap(t_map *map, irr::video::IVideoDriver *driver, irr::scene::ISceneManager *smgr)
  {
    allSpawners.push_back(new Spawner(BORDERMAP * (192) + (226), "spaw", "Squelette"));
    allSpawners[0]->add3dSpawner(smgr, driver, irr::core::vector3df(25.f, -70.f, -60.f));
    allSpawners.push_back(new Spawner(BORDERMAP * (183) + (226), "spaw_lezard", "Lezard"));
    allSpawners[1]->add3dSpawner(smgr, driver, irr::core::vector3df(25.f, -70.f, -100.f));
    allSpawners.push_back(new Spawner(BORDERMAP * (80) + (225), "spaw", "Squelette"));
    allSpawners[2]->add3dSpawner(smgr, driver, irr::core::vector3df(25.f, -70.f, -400.f));
    allSpawners.push_back(new Spawner(BORDERMAP * (80) + (330), "spaw", "Squelette"));
    allSpawners[3]->add3dSpawner(smgr, driver, irr::core::vector3df(-300.f, -70.f, -400.f));
    allSpawners.push_back(new Spawner(BORDERMAP * (80) + (400), "spaw", "Squelette"));
    allSpawners[4]->add3dSpawner(smgr, driver, irr::core::vector3df(-500.f, -70.f, -400.f));
    allSpawners.push_back(new Spawner(BORDERMAP * (28) + (35), "spaw_lezard", "Lezard"));
    allSpawners[5]->add3dSpawner(smgr, driver, irr::core::vector3df(600.f, -70.f, -550.f));
    allSpawners.push_back(new Spawner(BORDERMAP * (432) + (23), "spaw_lezard", "Lezard"));
    allSpawners[6]->add3dSpawner(smgr, driver, irr::core::vector3df(650.f, -70.f,650.f));
    allSpawners.push_back(new Spawner(BORDERMAP * (436) + (10), "spaw", "Squelette"));
    allSpawners[7]->add3dSpawner(smgr, driver, irr::core::vector3df(680.f, -70.f,680.f));
    allSpawners.push_back(new Spawner(BORDERMAP * (436) + (10), "spaw", "Squelette"));
    allSpawners[8]->add3dSpawner(smgr, driver, irr::core::vector3df(680.f, -70.f,680.f));

    allDoors.push_back(new Door(map, BORDERMAP * (98) + (269)));
    allDoors[0]->add3dDoor(driver, smgr, irr::core::vector3df(5, 19, 9), irr::core::vector3df(0, 90, 0), irr::core::vector3df(-160.f, -70.f, -380.f));
    allDoors.push_back(new Door(map, BORDERMAP * (123) + (120), true));
    allDoors[1]->add3dDoor(driver, smgr, irr::core::vector3df(5.f, 19.f, 9.f), irr::core::vector3df(0, 0, 0), irr::core::vector3df(375.f, -70.f, -280.f));
    allDoors.push_back(new Door(map, BORDERMAP * (217) + (173)));
    allDoors[2]->add3dDoor(driver, smgr, irr::core::vector3df(5.f, 19.f, 9.f), irr::core::vector3df(0, 90, 0), irr::core::vector3df(130.f, -70.f, -20.f));
    allDoors.push_back(new Door(map, BORDERMAP * (34) + (191)));
    allDoors[3]->add3dDoor(driver, smgr, irr::core::vector3df(5.f, 19.f, 9.f), irr::core::vector3df(0, 90, 0), irr::core::vector3df(80.f, -70.f, -560.f));
    allDoors.push_back(new Door(map, BORDERMAP * (34) + (321)));
    allDoors[4]->add3dDoor(driver, smgr, irr::core::vector3df(5.f, 19.f, 11.f), irr::core::vector3df(0, 90, 0), irr::core::vector3df(-326.f, -70.f, -560.f));
    allDoors.push_back(new Door(map, BORDERMAP * (299) + (307)));
    allDoors[5]->add3dDoor(driver, smgr, irr::core::vector3df(5.f, 19.f, 11.f), irr::core::vector3df(0, 90, 0), irr::core::vector3df(-270.f, -70.f, 230.f));
    allDoors.push_back(new Door(map, BORDERMAP * (299) + (391)));
    allDoors[6]->add3dDoor(driver, smgr, irr::core::vector3df(5.f, 19.f, 11.f), irr::core::vector3df(0, 90, 0), irr::core::vector3df(-530.f, -70.f, 230.f));
    allDoors.push_back(new Door(map, BORDERMAP * (356) + (211)));
    allDoors[7]->add3dDoor(driver, smgr, irr::core::vector3df(5.f, 19.f, 11.f), irr::core::vector3df(0, 90, 0), irr::core::vector3df(10.f, -70.f, 405.f));
    allDoors.push_back(new Door(map, BORDERMAP * (404) + (198), true));
    allDoors[8]->add3dDoor(driver, smgr, irr::core::vector3df(5.f, 19.f, 11.f), irr::core::vector3df(0, 0, 0), irr::core::vector3df(130.f, -70.f, 580.f));
    allDoors.push_back(new Door(map, BORDERMAP * (228) + (243), true));
    allDoors[9]->add3dDoor(driver, smgr, irr::core::vector3df(5.f, 19.f, 10.f), irr::core::vector3df(0, 0, 0), irr::core::vector3df(0.f, -70.f, 50.f));
    allDoors.push_back(new Door(map, BORDERMAP * (2) + (156), true));
    allDoors[10]->add3dDoor(driver, smgr, irr::core::vector3df(5.f, 19.f, 9.f), irr::core::vector3df(0, 0, 0), irr::core::vector3df(250.f, -70.f, -650.f));
    allDoors.push_back(new Door(map, BORDERMAP * (80) + (26)));
    allDoors[11]->add3dDoor(driver, smgr, irr::core::vector3df(5.f, 19.f, 19.f), irr::core::vector3df(0, 90, 0), irr::core::vector3df(510.f, -70.f, -435.f));
    allDoors.push_back(new Door(map, BORDERMAP * (284) + (68)));
    allDoors[12]->add3dDoor(driver, smgr, irr::core::vector3df(5.f, 19.f, 15.f), irr::core::vector3df(0, 90, 0), irr::core::vector3df(410.f, -70.f, 200.f));
    allDoors.push_back(new Door(map, BORDERMAP * (185) + (133)));
    allDoors[13]->add3dDoor(driver, smgr, irr::core::vector3df(5.f, 19.f, 9.f), irr::core::vector3df(0, 90, 0), irr::core::vector3df(250.f, -70.f, -120.f));

    allKeys.push_back(new Key);
    allKeys[0]->add3dKey(driver, smgr, irr::core::vector3df(20.f, -70.f, -20.f));
    allKeys.push_back(new Key);
    allKeys[1]->add3dKey(driver, smgr, irr::core::vector3df(-320.f, -70.f, 10.f));
    allKeys.push_back(new Key);
    allKeys[2]->add3dKey(driver, smgr, irr::core::vector3df(-280.f, -70.f, -50.f));
    allKeys.push_back(new Key);
    allKeys[3]->add3dKey(driver, smgr, irr::core::vector3df(680.f, -70.f, -640.f));
    allKeys.push_back(new Key);
    allKeys[4]->add3dKey(driver, smgr, irr::core::vector3df(690.f, -70.f, -50.f));
    allKeys.push_back(new Key);
    allKeys[5]->add3dKey(driver, smgr, irr::core::vector3df(690.f, -70.f, 470.f));
    allKeys.push_back(new Key);
    allKeys[6]->add3dKey(driver, smgr, irr::core::vector3df(40.f, -70.f, 35.f));
    allKeys.push_back(new Key);
    allKeys[7]->add3dKey(driver, smgr, irr::core::vector3df(-180.f, -70.f, 280.f));
    allKeys.push_back(new Key);
    allKeys[8]->add3dKey(driver, smgr, irr::core::vector3df(-100.f, -70.f, 280.f));
    allKeys.push_back(new Key);
    allKeys[9]->add3dKey(driver, smgr, irr::core::vector3df(320.f, -70.f, -510.f));
    allKeys.push_back(new Key);
    allKeys[10]->add3dKey(driver, smgr, irr::core::vector3df(-400.f, -70.f, -180.f));
    allKeys.push_back(new Key);
    allKeys[11]->add3dKey(driver, smgr, irr::core::vector3df(-680.f, -70.f, -650.f));

    allChest.push_back(new Chest());
    allChest[0]->add3dChest(driver, smgr, irr::core::vector3df(-280, -70, 10));
    allChest.push_back(new Chest());
    allChest[1]->add3dChest(driver, smgr, irr::core::vector3df(-280, -70, 50));
    allChest.push_back(new Chest());
    allChest[2]->add3dChest(driver, smgr, irr::core::vector3df(-320, -70, 50));
    allChest.push_back(new Chest());
    allChest[3]->add3dChest(driver, smgr, irr::core::vector3df(-280, -70, 90));
    allChest.push_back(new Chest());
    allChest[4]->add3dChest(driver, smgr, irr::core::vector3df(-320, -70, 90));
    allChest.push_back(new Chest());
    allChest[5]->add3dChest(driver, smgr, irr::core::vector3df(-280, -70, 130));
    allChest.push_back(new Chest());
    allChest[6]->add3dChest(driver, smgr, irr::core::vector3df(-320, -70, 130));
    allChest.push_back(new Chest());
    allChest[7]->add3dChest(driver, smgr, irr::core::vector3df(-320, -70, -50));
    allChest.push_back(new Chest());
    allChest[8]->add3dChest(driver, smgr, irr::core::vector3df(-280, -70, -90));
    allChest.push_back(new Chest());
    allChest[9]->add3dChest(driver, smgr, irr::core::vector3df(-320, -70, -90));
    allChest.push_back(new Chest());
    allChest[10]->add3dChest(driver, smgr, irr::core::vector3df(140, -70, -310));
    allChest.push_back(new Chest());
    allChest[11]->add3dChest(driver, smgr, irr::core::vector3df(30, -70, -505));
    allChest.push_back(new Chest());
    allChest[12]->add3dChest(driver, smgr, irr::core::vector3df(630, -70, -640));
    allChest.push_back(new Chest());
    allChest[13]->add3dChest(driver, smgr, irr::core::vector3df(680.f, -70.f, -100.f));
    allChest.push_back(new Chest());
    allChest[14]->add3dChest(driver, smgr, irr::core::vector3df(680.f, -70.f, -150.f));
    allChest.push_back(new Chest());
    allChest[15]->add3dChest(driver, smgr, irr::core::vector3df(680.f, -70.f, -200.f));
    allChest.push_back(new Chest());
    allChest[16]->add3dChest(driver, smgr, irr::core::vector3df(680.f, -70.f, 0.f));
    allChest.push_back(new Chest());
    allChest[17]->add3dChest(driver, smgr, irr::core::vector3df(640.f, -70.f, 470.f));
    allChest.push_back(new Chest());
    allChest[18]->add3dChest(driver, smgr, irr::core::vector3df(600.f, -70.f, 500.f));
    allChest.push_back(new Chest());
    allChest[19]->add3dChest(driver, smgr, irr::core::vector3df(560.f, -70.f, 530.f));
    allChest.push_back(new Chest());
    allChest[20]->add3dChest(driver, smgr, irr::core::vector3df(-630.f, -70.f, -650.f));
    allChest.push_back(new Chest());
    allChest[21]->add3dChest(driver, smgr, irr::core::vector3df(-680.f, -70.f, -600.f));
    allChest.push_back(new Chest());
    allChest[22]->add3dChest(driver, smgr, irr::core::vector3df(-630.f, -70.f, -550.f));
    allChest.push_back(new Chest());
    allChest[23]->add3dChest(driver, smgr, irr::core::vector3df(-680.f, -70.f, -500.f));
    allChest.push_back(new Chest());
    allChest[24]->add3dChest(driver, smgr, irr::core::vector3df(-630.f, -70.f, -450.f));
    allChest.push_back(new Chest());
    allChest[25]->add3dChest(driver, smgr, irr::core::vector3df(-340.f, -70.f, 460.f));
    allChest.push_back(new Chest());
    allChest[26]->add3dChest(driver, smgr, irr::core::vector3df(-230.f, -70.f, 460.f));

    allMeal.push_back(new Meal());
    allMeal[0]->add3dMeal(driver, smgr, irr::core::vector3df(100, -70, -310));
    allMeal.push_back(new Meal());
    allMeal[1]->add3dMeal(driver, smgr, irr::core::vector3df(180, -70, -310));
    allMeal.push_back(new Meal());
    allMeal[2]->add3dMeal(driver, smgr, irr::core::vector3df(580, -70, -640));
    allMeal.push_back(new Meal());
    allMeal[3]->add3dMeal(driver, smgr, irr::core::vector3df(530, -70, 560));
    allMeal.push_back(new Meal());
    allMeal[4]->add3dMeal(driver, smgr, irr::core::vector3df(-680, -70, -400));
    allMeal.push_back(new Meal());
    allMeal[5]->add3dMeal(driver, smgr, irr::core::vector3df(-283, -70, 450));
    exit.add3dExit(driver, smgr, irr::core::vector3df(-650, -70, 650));
  };

  std::vector<Door *>	&	getDoors()
  {
    return (allDoors);
  };

  std::vector<Key *>	&	getKeys()
  {
    return (allKeys);
  };

  std::vector<Spawner *> &	getSpawner()
  {
    return (allSpawners);
  };

  void				updateMap(irr::video::IVideoDriver *driver, irr::scene::ISceneManager * smgr, ICharacter &player, t_map *map, IMusic *backSound)
  {
    for (int j = 0; j < getSpawner().size(); j++)
      {
	for (int i = 0; i < getSpawner()[j]->getEnemies().size(); i++)
	  {
	    //getSpawner()[j]->getEnemies()[i]->moveLeft(0, map, getSpawner()[j]->getEnemies()[i]->isRunning());
	    getSpawner()[j]->getEnemies()[i]->ia(player, map);
	    getSpawner()[j]->getEnemies()[i]->getCollisionMgr().collisionTwoNodes(player.getMesh(), getSpawner()[j]->getEnemies()[i]->getMesh());
	    getSpawner()[j]->destroyEnemy(map, i, getSpawner()[j]->getEnemies()[i]->getCollisionMgr().getNbCol(), player);
	    getSpawner()[j]->setEnemyNo(player.projectileCollision(getSpawner()[j]->getEnemies()[i], map) * -1);
	  }
	player.projectileCollision(getSpawner()[j], map);
      }
    for (int i = 0; i < getKeys().size(); i++)
      {
	getKeys()[i]->getColMgr().collisionTwoNodes(player.getMesh(), getKeys()[i]->getMesh());
	if (getKeys()[i]->isTaken())
	  player.setKey(1);
      }
    for (int i = 0; i < getDoors().size(); i++)
      {
	if (getDoors()[i]->vertical() == true)
	  getDoors()[i]->isOpen(player, map, true, backSound);
	else
	  getDoors()[i]->isOpen(player, map, backSound);
      }
    for (int i = 0; i < allChest.size(); i++)
      {
	allChest[i]->getColMgr().collisionTwoNodes(player.getMesh(), allChest[i]->getMesh());
	allChest[i]->isTaken(player, backSound);
      }
    for (int i = 0; i < allMeal.size(); i++)
      {
	allMeal[i]->getColMgr().collisionTwoNodes(player.getMesh(), allMeal[i]->getMesh());
	allMeal[i]->isTaken(player, backSound);
      }
    exit.getColMgr().collisionTwoNodes(player.getMesh(), exit.getMesh());
    player.win(exit, driver);
  };

  ~Map() {};
};

#endif
