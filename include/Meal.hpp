//
// Key.hpp for  in /home/grandr_r/Cpp_Prog/cpp_indie_studio
//
// Made by 
// Login   <grandr_r@epitech.net>
//
// Started on  Mon May 23 10:36:50 2016 
// Last update Sun May 29 16:05:25 2016 croayn_v
//

#ifndef MEAL_HPP_
# define MEAL_HPP_

#include <irrlicht.h>
#include "ICharacter.hpp"
#include "IMusic.hpp"
#include "CollisionManager.hpp"

class 			Meal
{
  bool			isAvailable;
  CollisionManager	colmgr;
  irr::scene::IAnimatedMesh   *mesh;
  irr::scene::IAnimatedMeshSceneNode *modelNode;
public:
  Meal() : isAvailable(true) {};
  void			add3dMeal(irr::video::IVideoDriver *driver, irr::scene::ISceneManager *smgr, irr::core::vector3df const &position)
  {
    mesh = smgr->getMesh("ressources/BBQ.obj");
    modelNode = smgr->addAnimatedMeshSceneNode(mesh);
    if (modelNode)
      {
	modelNode->setScale(irr::core::vector3df(9, 9, 9));
        //modelNode->setPosition(irr::core::vector3df(-140.f, -70.f, 0.f));
	modelNode->setPosition(position);
        modelNode->setMaterialTexture(0, driver->getTexture("ressources/BBQ.png"));
        modelNode->getMaterial(0).Shininess = 150.f;
      }
  };

  CollisionManager	&getColMgr()
  {
    return (colmgr);
  };

  irr::scene::IAnimatedMeshSceneNode *	getMesh()
  {
    return (modelNode);
  };

  bool			isTaken(ICharacter & player, IMusic * backSound)
  {
    if (isAvailable == false)
      return (false);
    if (colmgr.getNbCol() > 1)
      {
	isAvailable = false;
	player.setLife(30);
	player.setScore(100);
	backSound->playSound(1);
	modelNode->setVisible(false);
	return (true);
      }
    return (false);
  };

  ~Meal() {};
};

#endif
