//
// BaseFrame.hpp for  in /home/grandr_r/Cpp_Prog/cpp_indie_studio
//
// Made by grandr_r
// Login   <grandr_r@epitech.net>
//
// Started on  Fri Apr 29 10:32:58 2016 grandr_r
// Last update Tue May 31 12:38:03 2016 
//

#ifndef BASEFRAME_HPP_
# define BASEFRAME_HPP_

#include <irrlicht.h>
#include <irrKlang.h>
#include <future>
#include <iostream>
#include <unistd.h>
#include "Key.hpp"
#include "ICharacter.hpp"
#include "ICamera.hpp"
#include "GetFromFile.hpp"
#include "IMenu.hpp"
#include "IEnemy.hpp"
#include "IMusic.hpp"
#include "Spawner.hpp"
#include "Door.hpp"
#include "CollisionManager.hpp"
#include "IProjectile.hpp"
#include "Map.hpp"
#include "PauseMenu.hpp"

#define KEY_ALIGN 35

class				BaseFrame : public irr::IEventReceiver
{
  irr::IrrlichtDevice		*device;
  bool				buttons[6];
  bool 				KeyIsDown[irr::KEY_KEY_CODES_COUNT];
  bool				isRunning;
  t_map	*			map;
  ICharacter			player;
  ICamera			*camera;
  int				lastKeyPressed;
  bool				isMusic;
  irr::video::IVideoDriver	*driver;
  irr::scene::ISceneManager	*smgr;
  irr::gui::IGUIEnvironment	*guienv;
  bool				isPaused;
  IMusic      *     		backSound;
  Map 				mapall;
  irr::video::ITexture		*irrlichtBack;
public:
  BaseFrame()
  {
    isPaused = false;
    lastKeyPressed = -1;
    isRunning = false;
    for (irr::u32 i = 0; i < irr::KEY_KEY_CODES_COUNT; ++i)
      KeyIsDown[i] = false;
  };

  virtual bool IsKeyDown(irr::EKEY_CODE keyCode) const
  {
    return KeyIsDown[keyCode];
  }

  virtual bool	OnEvent(const irr::SEvent & event)
  {
    if (event.EventType == irr::EET_KEY_INPUT_EVENT &&
	event.KeyInput.Key == irr::KEY_ESCAPE &&
	event.KeyInput.PressedDown == false)
      {
	device->closeDevice();
	return (true);
      }
    if (event.EventType == irr::EET_KEY_INPUT_EVENT &&
	event.KeyInput.Key == irr::KEY_SPACE &&
	event.KeyInput.PressedDown == false)
      {
	player.setProjectile(new IProjectile(player.getFuncNo()), driver, smgr);
      }
    if (event.EventType == irr::EET_KEY_INPUT_EVENT)
      {
	KeyIsDown[event.KeyInput.Key] = event.KeyInput.PressedDown;
	lastKeyPressed = event.KeyInput.Key;
      }
    if (event.EventType == irr::EET_KEY_INPUT_EVENT &&
	event.KeyInput.Key == irr::KEY_RETURN &&
	event.KeyInput.PressedDown == false)
      {
	isPaused = !isPaused;
	PauseMenu(isPaused, driver, smgr, guienv, device, backSound);
      }
    return (false);
  };

  void			move(irr::scene::IAnimatedMeshSceneNode *modelNode)
  {
    int			sum = 0;

    for (int i = 37; i <= 40; i++)
      if (IsKeyDown(static_cast<irr::EKEY_CODE>(i)))
	{
	  if (IsKeyDown(irr::KEY_LEFT))
	    sum += (i - 1);
	  else
	    sum += (i);
	}
    sum -= 35;
    if (sum >= 38)
      sum -= 32;
    player.moveSelector(sum, map, camera->getPos(), camera->getTarget(), isRunning);
  }

  void			gameOver()
  {
    irr::video::ITexture	*irrlichtBack = driver->getTexture("ressources/gameover.jpg");
    driver->beginScene(true, true, irr::video::SColor(0, 0, 0, 0));
    if (irrlichtBack)
      driver->draw2DImage(irrlichtBack, irr::core::position2d<int>(0, 0));
    driver->endScene();
    backSound->playLastSound("ressources/gameover.wav", 2.f, device);
  };

  void			updateScene(irr::scene::IAnimatedMeshSceneNode *modelNode, int &lastFPS)
  {
    std::string my = "Score: " + std::to_string(player.getScore()) + "\n\nLife: " + std::to_string(player.getLife()) + "\n\nKeys: " + std::to_string(player.asKey());
    std::wstring tmp(my.begin(), my.end());
    const wchar_t * test = tmp.c_str();
    guienv->addStaticText(test, irr::core::rect<int>(10, 10, 230, 120), true, true);
    mapall.updateMap(driver, smgr, player, map, backSound);
    move(modelNode);
    player.projectileMove(map);
    if (IsKeyDown(irr::KEY_UP) || IsKeyDown(irr::KEY_LEFT) || IsKeyDown(irr::KEY_DOWN) || IsKeyDown(irr::KEY_RIGHT))
      isRunning = true;
    else
      {
	if (isRunning == true)
	  player.setMeshAnim(irr::scene::EMAT_STAND);
	isRunning = false;
      }
    camera->updatePosition(camera->getPos());
    camera->updateTarget(camera->getTarget());
    driver->beginScene(true, true, irr::video::SColor(0, 0, 0, 0));
    if (irrlichtBack)
      driver->draw2DImage(irrlichtBack, irr::core::position2d<int>(0, 0));
    smgr->drawAll();
    guienv->drawAll();
    driver->endScene();
    int fps = driver->getFPS();

    if (player.getLife() <= 0)
      gameOver();

    guienv->clear();
    if (lastFPS != fps)
      {
	for (int i = 0; i < mapall.getSpawner().size(); i++)
	  {
	    if (abs(mapall.getSpawner()[i]->getMesh()->getPosition().X - player.getMesh()->getPosition().X) <= 300)
	      {
		mapall.getSpawner()[i]->spawnEnemy(smgr, driver, map);
		mapall.getSpawner()[i]->setEnemyNo(1);
	      }
	  }
	irr::core::stringw str = L"Irrlicht Engine - Map example [";
	str += driver->getName();
	str += "] FPS:";
	str += fps;

	device->setWindowCaption(str.c_str());
	lastFPS = fps;
      }
  };

  void			play(IMenu const & menu)
  {
    isMusic = menu.getButtons()[MUSIC + ALIGN];
    device = createDevice(menu.getDriverType(), irr::core::dimension2d<irr::u32>(1920, 1080), 16, menu.getButtons()[FULLSCREEN + ALIGN], false, menu.getButtons()[VSYNC + ALIGN], this);

    driver = device->getVideoDriver();
    smgr = device->getSceneManager();
    guienv = device->getGUIEnvironment();

    irr::gui::IGUISkin          *newskin = guienv->createSkin(irr::gui::EGST_BURNING_SKIN);
    guienv->setSkin(newskin);
    newskin->drop();

    irr::gui::IGUIFont          *font = guienv->getFont("ressources/font1.bmp");
    if (font)
      guienv->getSkin()->setFont(font);

    irr::scene::IAnimatedMesh	*mesh = smgr->getMesh("ressources/map_lv_1-1.obj");
    irr::scene::IAnimatedMeshSceneNode *modelNode = smgr->addAnimatedMeshSceneNode(mesh);
    GetFromFile		file("ressources/demo_final");
    modelNode->setMaterialFlag(irr::video::EMF_BACK_FACE_CULLING, false);

    irr::scene::IAnimatedMesh   *mesh2 = smgr->getMesh("ressources/sol_lv_1.obj");
    irr::scene::IAnimatedMeshSceneNode *modelNode2 = smgr->addAnimatedMeshSceneNode(mesh2);
    modelNode2->setMaterialFlag(irr::video::EMF_BACK_FACE_CULLING, false);

    smgr->setAmbientLight(irr::video::SColor(0, 255, 255, 255));
    irr::scene::ILightSceneNode* mySpotLight = smgr->addLightSceneNode();
    mySpotLight->setPosition(irr::core::vector3df(0.0f, 50.f, 100.0f));
    irr::video::SLight spotLightData;
    spotLightData.Type = irr::video::ELT_POINT;
    spotLightData.Attenuation = irr::core::vector3df(4.f, 0, 0);
    mySpotLight->setLightData(spotLightData);

    if (modelNode)
      {
	modelNode->setScale(irr::core::vector3df(25.5f, 25, 25.f));
	modelNode->setPosition(irr::core::vector3df(-85.f, -100.f, 50.f));
	modelNode->setMaterialTexture(0, driver->getTexture("ressources/mur_jaune.jpg"));
	modelNode->getMaterial(0).Shininess = 150.f;
      }

    if (modelNode2)
      {
        modelNode2->setScale(irr::core::vector3df(25.5f, 25, 25));
        modelNode2->setPosition(irr::core::vector3df(-85.f, -100.f, 50.f));
        modelNode2->getMaterial(0).Shininess = 130.f;
      }

    player.setPos(BORDERMAP * (BORDERMAP / 2) + (BORDERMAP /2));
    player.addNewMesh(smgr, "ressources/" + menu.getCharName() + ".md2", "ressources/" + menu.getCharName() + ".jpg", driver);
    player.setMeshPosition(irr::core::vector3df(0.f, -45.f, 60.f));
    player.setMeshAnim(irr::scene::EMAT_STAND);

    camera = new ICamera(-20, 180, 290, -20, -20, 190, smgr);
    irrlichtBack = driver->getTexture("ressources/fon_indie.jpg");
    driver->setTextureCreationFlag(irr::video::ETCF_CREATE_MIP_MAPS, false);

    int			lastFPS = -1;

    backSound = new IMusic("ressources/Music/1.wav", true, isMusic);
    backSound->playSound(3);

    map = file.getMap();
    map[player.getPos()].tiletype = PLAYERCHAR;

    mapall.initMap(map, driver, smgr);
    while (device->run())
      {
	if (device->isWindowActive())
	  updateScene(modelNode, lastFPS);
	else
	  device->yield();
      }
    device->drop();
  }

  ~BaseFrame()
  {
  };
};

#endif
