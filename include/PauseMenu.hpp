//
// IMenu.hpp for  in /home/grandr_r/Cpp_Prog/cpp_indie_studio
//
// Made by 
// Login   <grandr_r@epitech.net>
//
// Started on  Sun May 22 10:12:27 2016 
// Last update Sat May 28 17:02:11 2016 
//

#ifndef PAUSEMENU_HPP_
#define PAUSEMENU_HPP_

#include <irrlicht.h>
#include <string>
#include "IMusic.hpp"

class			PauseMenu : public irr::IEventReceiver
{
  irr::gui::IGUIButton          *startButton;
  bool		&		isPaused;
public:
  PauseMenu(bool & other, irr::video::IVideoDriver *driver, irr::scene::ISceneManager *smgr, irr::gui::IGUIEnvironment *guienv, irr::IrrlichtDevice *device, IMusic * music) : isPaused(other)
  {
    music->pauseSound();
    const irr::s32              leftX = 610;

    irr::gui::IGUITabControl    *tabctrl = guienv->addTabControl(irr::core::rect<int>(leftX, 645, leftX + 700, 645 + 265), 0, true, true);

    irr::gui::IGUITab           *optTab = tabctrl->addTab(L"Pause");

    //startButton = guienv->addButton(irr::core::rect<int>(irr::core::position2d<int>(leftX + 50, 645 + 30), irr::core::position2d<int>(leftX + 80, 645 + 90)), optTab, 2, L"Start Demo");

    irr::video::ITexture        *irrlichtBack = driver->getTexture("ressources/pause.jpg");
    driver->setTextureCreationFlag(irr::video::ETCF_CREATE_MIP_MAPS, false);

    while (device->run())
      {
        if (device->isWindowActive())
          {
            driver->beginScene(false, true, irr::video::SColor(0, 0, 0, 0));
            if (irrlichtBack)
              driver->draw2DImage(irrlichtBack, irr::core::position2d<int>(0, 0));
            guienv->drawAll();
            driver->endScene();
	    if (!isPaused)
	      {
		music->playSoundAgain();
		break;
	      }
          }
      }
  };

  virtual bool  OnEvent(const irr::SEvent & event)
  {
    //if (event.EventType == irr::EET_KEY_INPUT_EVENT &&
    //  event.KeyInput.Key == irr::KEY_ESCAPE &&
    //  event.KeyInput.PressedDown == false)
    //{
    //	MenuDevice->closeDevice();
    //	MenuDevice->drop();
    //	exit(0);
    //}
    if (event.EventType == irr::EET_KEY_INPUT_EVENT &&
        event.KeyInput.Key == irr::KEY_RETURN &&
        event.KeyInput.PressedDown == false)
      isPaused = !isPaused;
    return (false);
  };

  ~PauseMenu()
  {
  };
};

#endif
