//
// GetFromFile.hpp for  in /home/grandr_r/Cpp_Prog/cpp_indie_studio
//
// Made by 
// Login   <grandr_r@epitech.net>
//
// Started on  Thu May 19 10:08:16 2016 
// Last update Mon May 23 19:22:27 2016 croayn_v
//

#ifndef GETFROMFILE_HPP_
#define GETFROMFILE_HPP_

#include <fstream>

#define BORDERMAP 470
#define PLAYERCHAR '>'
#define BORDERCHAR '+'
#define EMPTYCHAR 'o'
#define ENEMYCHAR '/'
#define DOORCHAR 'D'

typedef struct	s_map
{
  int		x;
  int		y;
  int		tiletype;
}		t_map;

class 			GetFromFile
{
  t_map			*map;
public:
  GetFromFile(std::string const & filename)
  {
    std::ifstream	fin(filename.c_str());
    int			y = 0;
    int			i = 0;
    char		ch;

    map = new t_map[BORDERMAP * BORDERMAP];
    if (!fin)
      {
	std::cerr << "Error : could not load ressource/map" << std::endl;
	return;
      }
    while (fin >> std::skipws >> ch)
      {
        if (i % BORDERMAP == 0)
          y++;
        map[i].x = i % BORDERMAP;
        map[i].y = y;
        if (ch != ' ')
          {
            map[i].tiletype = (int)ch;
            i++;
          }
      }
    fin.close();
  };

  void			displayMap()
  {
    for (int i = 0; i < (BORDERMAP * BORDERMAP); i++)
      {
	if (i % (BORDERMAP) == 0)
	  std::cout << std::endl;
	std::cout << (char)map[i].tiletype;
      }
    std::cout << std::endl;
  };

  t_map	*		getMap()
  {
    return (map);
  }

  ~GetFromFile()
  {
    delete[] map;
  };
};

#endif
