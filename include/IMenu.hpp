//
// IMenu.hpp for  in /home/grandr_r/Cpp_Prog/cpp_indie_studio
//
// Made by 
// Login   <grandr_r@epitech.net>
//
// Started on  Sun May 22 10:12:27 2016 
// Last update Tue May 31 12:33:46 2016 
//

#ifndef IMENU_HPP_
#define IMENU_HPP_

#include <irrlicht.h>
#include <irrKlang.h>
#include <string>
#include "IMusic.hpp"

#define WIN_WIDTH 1280
#define WIN_HEIGHT 720

#define ALIGN 3
#define FULLSCREEN 0
#define MUSIC 1
#define SHADOWS 2
#define VSYNC 3
#define AA 4

class			IMenu : public irr::IEventReceiver
{
  bool				*buttons;
  irr::IrrlichtDevice           *MenuDevice;
  irr::video::E_DRIVER_TYPE     driverType;
  irr::gui::IGUIButton          *startButton;
  irr::s32                      selected;
  irr::s32			character;
  std::string			choosedChar;
public:
  IMenu() : driverType(irr::video::EDT_OPENGL), startButton(0), selected(0), character(0)
  {
    std::string			charName[5] = {"faerie", "Blade", "drfreak", "sydney", "Potator"};
    buttons = new bool[6];
    buttons[FULLSCREEN + ALIGN] = (false);
    buttons[MUSIC + ALIGN] = (true);
    buttons[SHADOWS + ALIGN] = (false);
    buttons[VSYNC + ALIGN] = (false);
    buttons[AA + ALIGN] = (false);

    irr::video::E_DRIVER_TYPE   tmpDriver = irr::video::EDT_OPENGL;
    MenuDevice = createDevice(tmpDriver, irr::core::dimension2d<irr::u32>(WIN_WIDTH, WIN_HEIGHT), 16, false, false, false, this);
    if (!MenuDevice)
      {
	std::cerr << "Error : cannot start without DISPLAY ENV Variable" << std::endl;
        exit(-1);
      }
    irr::video::IVideoDriver    *driver = MenuDevice->getVideoDriver();
    irr::scene::ISceneManager   *smgr = MenuDevice->getSceneManager();
    irr::gui::IGUIEnvironment   *guienv = MenuDevice->getGUIEnvironment();
    irr::core::stringw          str = "Irrlicht Demo #1";
    str += MenuDevice->getVersion();
    MenuDevice->setWindowCaption(str.c_str());
    irr::gui::IGUISkin          *newskin = guienv->createSkin(irr::gui::EGST_BURNING_SKIN);
    guienv->setSkin(newskin);
    newskin->drop();

    irr::gui::IGUIFont          *font = guienv->getFont("ressources/font1.bmp");
    if (font)
      guienv->getSkin()->setFont(font);

    const irr::s32              leftX = 110;

    irr::gui::IGUITabControl    *tabctrl = guienv->addTabControl(irr::core::rect<int>(leftX, 345, 700, WIN_HEIGHT - 65), 0, true, true);

    irr::gui::IGUITab           *optTab = tabctrl->addTab(L"Options");
    irr::gui::IGUITab           *characterTab = tabctrl->addTab(L"Choose Your Charachter");
    irr::gui::IGUITab           *aboutTab = tabctrl->addTab(L"About");

    irr::gui::IGUIListBox       *box = guienv->addListBox(irr::core::rect<int>(10, 10, 160, 100), optTab, 1);
    box->addItem(L"OpenGL");
    box->addItem(L"Direct3D 9.0");
    box->setSelected(selected);

    irr::gui::IGUIListBox       *box2 = guienv->addListBox(irr::core::rect<int>(10, 10, 160, 100), characterTab, 1);
    box2->addItem(L"Faerie");
    box2->addItem(L"Blade");
    box2->addItem(L"Dr Freak");
    box2->addItem(L"Sydney");
    box2->addItem(L"Potator");
    box2->setSelected(character);

    startButton = guienv->addButton(irr::core::rect<int>(390, 240, 550, 260), optTab, 2, L"Start Demo");

    const irr::s32 d = 60;

    guienv->addCheckBox(buttons[FULLSCREEN + ALIGN], irr::core::rect<int>(20, 85 + d, 130, 110 + d), optTab, 3, L"fullscreen");
    guienv->addCheckBox(buttons[MUSIC + ALIGN], irr::core::rect<int>(135, 85 + d, 245, 110 + d), optTab, 4, L"Music & Sfx");
    guienv->addCheckBox(buttons[SHADOWS + ALIGN], irr::core::rect<int>(20, 110 + d, 135, 135 + d), optTab, 5, L"Realtime shadows");
    guienv->addCheckBox(buttons[VSYNC + ALIGN], irr::core::rect<int>(20, 160 + d, 230, 185 + d), optTab, 6, L"Vectical Sync");
    guienv->addCheckBox(buttons[AA + ALIGN], irr::core::rect<int>(20, 185 + d, 230, 210 + d), optTab, 7, L"Antialiasing");

    const wchar_t               *text2 = L"This is the first demo with this engine so be nice please !";

    guienv->addStaticText(text2, irr::core::rect<int>(10, 10, 230, 320), true, true, aboutTab);

    irr::scene::IAnimatedMesh   *mesh[5];
    mesh[0] = smgr->getMesh("ressources/faerie.md2");
    mesh[1] = smgr->getMesh("ressources/Blade.md2");
    mesh[2] = smgr->getMesh("ressources/drfreak.md2");
    mesh[3] = smgr->getMesh("ressources/sydney.md2");
    mesh[4] = smgr->getMesh("ressources/Potator.md2");
    irr::scene::IAnimatedMeshSceneNode *modelNode[5];
    modelNode[0]= smgr->addAnimatedMeshSceneNode(mesh[0]);
    modelNode[1]= smgr->addAnimatedMeshSceneNode(mesh[1]);
    modelNode[2]= smgr->addAnimatedMeshSceneNode(mesh[2]);
    modelNode[3]= smgr->addAnimatedMeshSceneNode(mesh[3]);
    modelNode[4]= smgr->addAnimatedMeshSceneNode(mesh[4]);

    if (modelNode[0])
      {
        modelNode[0]->setPosition(irr::core::vector3df(-40.f, -30.f, 90.f));
        modelNode[0]->setMaterialTexture(0, driver->getTexture("ressources/faerie.jpg"));
        modelNode[0]->getMaterial(0).Shininess = 50.f;
        modelNode[0]->getMaterial(0).NormalizeNormals = false;
        modelNode[0]->setMD2Animation(irr::scene::EMAT_STAND);
	modelNode[0]->setVisible(false);
      }

    if (modelNode[1])
      {
        modelNode[1]->setPosition(irr::core::vector3df(-40.f, -30.f, 90.f));
        modelNode[1]->setMaterialTexture(0, driver->getTexture("ressources/Blade.jpg"));
        modelNode[1]->getMaterial(0).Shininess = 50.f;
        modelNode[1]->getMaterial(0).NormalizeNormals = false;
        modelNode[1]->setMD2Animation(irr::scene::EMAT_STAND);
	modelNode[1]->setVisible(false);
      }

    if (modelNode[2])
      {
        modelNode[2]->setPosition(irr::core::vector3df(-40.f, -30.f, 90.f));
        modelNode[2]->setMaterialTexture(0, driver->getTexture("ressources/drfreak.jpg"));
        modelNode[2]->getMaterial(0).Shininess = 50.f;
        modelNode[2]->getMaterial(0).NormalizeNormals = false;
        modelNode[2]->setMD2Animation(irr::scene::EMAT_STAND);
	modelNode[2]->setVisible(false);
      }

    if (modelNode[3])
      {
        modelNode[3]->setPosition(irr::core::vector3df(-40.f, -30.f, 90.f));
        modelNode[3]->setMaterialTexture(0, driver->getTexture("ressources/sydney.jpg"));
        modelNode[3]->getMaterial(0).Shininess = 50.f;
        modelNode[3]->getMaterial(0).NormalizeNormals = false;
        modelNode[3]->setMD2Animation(irr::scene::EMAT_STAND);
	modelNode[3]->setVisible(false);
      }

    if (modelNode[4])
      {
        modelNode[4]->setPosition(irr::core::vector3df(-40.f, -30.f, 90.f));
        modelNode[4]->setMaterialTexture(0, driver->getTexture("ressources/Potator.jpg"));
        modelNode[4]->getMaterial(0).Shininess = 50.f;
        modelNode[4]->getMaterial(0).NormalizeNormals = false;
        modelNode[4]->setMD2Animation(irr::scene::EMAT_STAND);
	modelNode[4]->setVisible(false);
      }

    smgr->setAmbientLight(irr::video::SColorf(.7f, .7f, .7f));

    smgr->addCameraSceneNode(0, irr::core::vector3df(45, 0, 0), irr::core::vector3df(0, 0, 10));
    irr::video::ITexture        *irrlichtBack = driver->getTexture("ressources/back2.jpg");
    driver->setTextureCreationFlag(irr::video::ETCF_CREATE_MIP_MAPS, false);

    while (MenuDevice->run())
      {
        if (MenuDevice->isWindowActive())
          {
	    for (int i = 0; i < 5; i++)
	      modelNode[i]->setVisible(false);
	    modelNode[character]->setVisible(true);
            driver->beginScene(false, true, irr::video::SColor(0, 0, 0, 0));
            if (irrlichtBack)
              driver->draw2DImage(irrlichtBack, irr::core::position2d<int>(0, 0));
            smgr->drawAll();
            guienv->drawAll();
            driver->endScene();
          }
      }
    if (selected == 0)
      driverType = irr::video::EDT_OPENGL;
    else if (selected == 1)
      driverType = irr::video::EDT_DIRECT3D9;
    choosedChar = charName[character];
    MenuDevice->drop();
  };

  virtual bool  OnEvent(const irr::SEvent & event)
  {
    if (event.EventType == irr::EET_KEY_INPUT_EVENT &&
        event.KeyInput.Key == irr::KEY_ESCAPE &&
        event.KeyInput.PressedDown == false)
      {
	MenuDevice->closeDevice();
	MenuDevice->drop();
	exit(0);
      }
    if (event.EventType == irr::EET_GUI_EVENT)
      {
        if (event.GUIEvent.EventType == irr::gui::EGET_BUTTON_CLICKED)
          {
	    MenuDevice->closeDevice();
            return (true);
          }
	irr::s32 id = event.GUIEvent.Caller->getID();
	if (id == 1 && event.GUIEvent.EventType == irr::gui::EGET_LISTBOX_CHANGED ||
	    event.GUIEvent.EventType == irr::gui::EGET_LISTBOX_SELECTED_AGAIN)
	  {
	    character = ((irr::gui::IGUIListBox*)event.GUIEvent.Caller)->getSelected();
	  }
        if (event.GUIEvent.EventType == irr::gui::EGET_CHECKBOX_CHANGED)
          buttons[id] = ((irr::gui::IGUICheckBox*)event.GUIEvent.Caller)->isChecked();
      }
    return (false);
  };

  bool		 *	getButtons() const
  {
    return (buttons);
  };

  irr::video::E_DRIVER_TYPE     getDriverType() const
  {
    return (driverType);
  };

  void                          load()
  {
    irr::IrrlichtDevice           *device;
    device = createDevice(driverType, irr::core::dimension2d<irr::u32>(1920, 1080), 16, buttons[FULLSCREEN + ALIGN], false, buttons[VSYNC + ALIGN], this);
    irr::video::IVideoDriver       *driver = device->getVideoDriver();
    irr::scene::ISceneManager   *smgr = device->getSceneManager();
    irr::gui::IGUIEnvironment   *guienv = device->getGUIEnvironment();

    irr::video::ITexture        *irrlichtBack[6];

    irrlichtBack[0] = driver->getTexture("ressources/1.jpg");
    irrlichtBack[1] = driver->getTexture("ressources/1,5.jpg");
    irrlichtBack[2] = driver->getTexture("ressources/2.jpg");
    irrlichtBack[3] = driver->getTexture("ressources/2,5.jpg");
    irrlichtBack[4] = driver->getTexture("ressources/3.jpg");

    IMusic              backSound("ressources/cube.wav", false, buttons[MUSIC + ALIGN]);
    while (device->run())
      {
        if (device->isWindowActive())
          {
            driver->beginScene(false, true, irr::video::SColor(0, 0, 0, 0));
	    for (int j = 0; j < 4; j++)
	      for (int i = 0; i < 5; i++)
		{
		  if (irrlichtBack[i])
		    driver->draw2DImage(irrlichtBack[i], irr::core::position2d<int>(0, 0));
		  smgr->drawAll();
		  guienv->drawAll();
		  usleep(70000);
		  driver->endScene();
		}
            break;
          }
      }
    device->drop();
  };

  std::string const & getCharName() const
  {
    return (choosedChar);
  };

  ~IMenu()
  {
    delete[] buttons;
  };
};

#endif
