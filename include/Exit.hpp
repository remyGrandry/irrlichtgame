//
// Key.hpp for  in /home/grandr_r/Cpp_Prog/cpp_indie_studio
//
// Made by 
// Login   <grandr_r@epitech.net>
//
// Started on  Mon May 23 10:36:50 2016 
// Last update Mon May 30 13:28:31 2016 
//

#ifndef EXIT_HPP_
# define EXIT_HPP_

#include <irrlicht.h>
#include "CollisionManager.hpp"

class 			Exit
{
  bool			isAvailable;
  CollisionManager	colmgr;
  irr::scene::IAnimatedMesh   *mesh;
  irr::scene::IAnimatedMeshSceneNode *modelNode;
public:
  Exit() : isAvailable(true) {};
  void			add3dExit(irr::video::IVideoDriver *driver, irr::scene::ISceneManager *smgr, irr::core::vector3df const &position)
  {
    mesh = smgr->getMesh("ressources/spawn.obj");
    modelNode = smgr->addAnimatedMeshSceneNode(mesh);
    if (modelNode)
      {
	modelNode->setScale(irr::core::vector3df(2, 2, 2));
        //modelNode->setPosition(irr::core::vector3df(-140.f, -70.f, 0.f));
	modelNode->setPosition(position);
        modelNode->setMaterialTexture(0, driver->getTexture("ressources/exit.png"));
        modelNode->getMaterial(0).Shininess = 150.f;
      }
  };

  CollisionManager	&getColMgr()
  {
    return (colmgr);
  };

  irr::scene::IAnimatedMeshSceneNode *	getMesh()
  {
    return (modelNode);
  };

  bool			isTaken()
  {
    if (isAvailable == false)
      return (false);
    if (colmgr.getNbCol() > 1)
      {
	isAvailable = false;
	modelNode->setVisible(false);
	return (true);
      }
    return (false);
  };

  ~Exit() {};
};

#endif
